package com.papb.moneyapp

import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class Helper {

    fun dateFormatter(dateString: String) : String{
        val inputFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val outputFormat = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())

        // Ubah string tanggal menjadi objek Date

        val date = inputFormat.parse(dateString)

        // Ubah objek Date menjadi string tanggal dengan format yang diinginkan
        val formattedDate = outputFormat.format(date)


        return formattedDate.toString()
    }

    fun currency(amountString : String) : String{
        val amount = amountString.toDouble()

        val formatter = NumberFormat.getCurrencyInstance(Locale("id", "ID"))

        return formatter.format(amount)
    }
}