package com.papb.moneyapp

import android.content.ContentValues.TAG
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.papb.moneyapp.firebase.FirebaseAuth
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.Query

import com.papb.moneyapp.firebase.DbRealtime
import com.papb.moneyapp.model.Transaction
import com.papb.moneyapp.Helper
import java.text.NumberFormat
import java.util.EventListener
import java.util.Locale

class MainActivity : AppCompatActivity() {
    private lateinit var btnLogout : Button
    private lateinit var btnAddData : Button
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter : FinancialAdapter
    private lateinit var amountTextView: TextView
    private var totalIncome: Double = 0.0
    private var totalExpense: Double = 0.0
    private var transactionList: ArrayList<Transaction> = arrayListOf()
    val firebaseAuth = FirebaseAuth()
    private val database = DbRealtime()
    private val helper = Helper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadDataFromFirebase()

        btnLogout = findViewById(R.id.btn_logout)
        btnAddData = findViewById(R.id.btn_addTrans)
        amountTextView = findViewById(R.id.amountTextView)

        val toolbar = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white))

        recyclerView= findViewById(R.id.financialRecyclerView)
        adapter = FinancialAdapter(transactionList)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        btnLogout.setOnClickListener{
            firebaseAuth.logout()
            startActivity(Intent(this, LoginActivity::class.java))
        }

        btnAddData.setOnClickListener{
            startActivity(Intent(this, AddDataActivity::class.java))

        }
    }
    private fun loadDataFromFirebase() {
        val db = database.database
        var uid = firebaseAuth.auth.currentUser!!.uid
        // Read data from Firebase
        db.collection("transaction")
            .whereEqualTo("uid", uid)
            .orderBy("date", Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { documents ->
                transactionList.clear()
                totalIncome = 0.0
                totalExpense = 0.0

                // Loop through documents and add to transactionList
                for (document in documents) {
                    val transaction = document.toObject(Transaction::class.java)
                    transactionList.add(transaction)
                    transaction.docId = document.id

                    if (transaction.type == "Income") {
                        totalIncome += transaction.nominal.toDouble()
                    } else if (transaction.type == "Expense") {
                        totalExpense += transaction.nominal.toDouble()
                    }

                    Log.d(TAG, transaction.toString())
                }
                Log.d("Firestore", "Transaction data retrieved successfully $uid")
                if (::adapter.isInitialized) {
                    adapter.notifyDataSetChanged()
                } else {
                    // If not initialized, initialize and set the adapter
                    adapter = FinancialAdapter(transactionList)
                    recyclerView.adapter = adapter
                }

                Log.d("TransactionList", "Size of transactionList: ${transactionList.size}")

                val totalAmount = totalIncome - totalExpense
                val formattedTotalAmount = helper.currency(totalAmount.toString())
                amountTextView.text = formattedTotalAmount
            }
            .addOnFailureListener { exception ->
                // Handle error
                Log.e("Firestore", "Error retrieving transaction data", exception)
            }
    }


     fun deleteItemFromDatabase (position: Int, docId: String) {
        val db = database.database
        val collectionRef = db.collection("transaction")

        collectionRef
            .document(docId).delete()
            .addOnSuccessListener {
                loadDataFromFirebase()
            }
            .addOnFailureListener { e ->
                Log.e("Firestore", "Error deleting document", e)
                Toast.makeText(this, "Failed to delete item", Toast.LENGTH_SHORT).show()
            }
    }





}