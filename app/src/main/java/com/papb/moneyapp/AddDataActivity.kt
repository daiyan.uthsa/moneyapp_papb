package com.papb.moneyapp

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import com.papb.moneyapp.firebase.DbRealtime
import com.papb.moneyapp.firebase.FirebaseAuth
import com.papb.moneyapp.model.Transaction

class AddDataActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private lateinit var etTanggal: TextInputEditText
    private lateinit var keterangan : TextInputEditText
    private lateinit var jumlahUang : TextInputEditText
    private lateinit var tvActivity: TextView
    private val calendar = Calendar.getInstance()


    private val firebaseAuth = FirebaseAuth()
    private val db = DbRealtime()

    private lateinit var transacType : String
    lateinit var type:String
    lateinit var tanggal : String
    lateinit var nominalStr : String
    lateinit var desc : String





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_data)

        tvActivity = findViewById(R.id.tvActivity)
        etTanggal = findViewById(R.id.etTanggal)
        keterangan = findViewById(R.id.desc)
        jumlahUang = findViewById(R.id.etNominal)

        val position = intent.getIntExtra("position", 0)
        val uid = intent.getStringExtra("uid")
        val docId = intent.getStringExtra("docId")
        etTanggal.setText(intent.getStringExtra("date"))
        keterangan.setText(intent.getStringExtra("desc"))
        jumlahUang.setText(intent.getStringExtra("nominal"))
        transacType = intent.getStringExtra("type").toString()

        if (uid!=null){
            tvActivity.text = "Edit Transaksi"
        }else{
            tvActivity.text = "Menu Tambah Data"
        }

        val transType = resources.getStringArray(R.array.type)
        val adapter = ArrayAdapter(this, R.layout.dropdown_item, transType)

        // Get spinner view and set adapter
        val spinner = findViewById<Spinner>(R.id.dropdown_type)
        spinner.adapter = adapter

        spinner.onItemSelectedListener = this


        // Menangani klik pada TextInputEditText etTanggal
        etTanggal.setOnClickListener {
            showDatePickerDialog()
        }

        val btnSave = findViewById<Button>(R.id.btnSimpan)
        btnSave.setOnClickListener {
            type = transacType
            tanggal = etTanggal.text.toString()
            nominalStr = jumlahUang.text.toString()
            desc = if(!keterangan.text.isNullOrEmpty()){
                keterangan.text.toString()
            }else{
                "-"
            }

            if (!etTanggal.text.isNullOrEmpty() && !jumlahUang.text.isNullOrEmpty()){

                if (uid!=null){
                    var transac  =
                        docId?.let { it1 -> Transaction("",type, tanggal, nominalStr, desc, it1) }
                    if (transac != null) {
                        DbRealtime().updateDataInFirestore(position, uid, transac){callback ->
                            if (callback){
                                Log.d("Firebase", "Data berhasil update di Firebase")
                                startActivity(Intent( this, MainActivity::class.java
                                ))
                                finish()
                            }else{
                                Log.w("Firebase", "Error saat memperbarui data di Firebase")
                            }
                        }
                    }
                }else{
                    saveDataToFirebase()
                }

            }else{
                Toast.makeText(this, "Tanggal dan jumlah uang tidak boleh kosong", Toast.LENGTH_SHORT).show()
            }

        }

    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        // Handle spinner item selection
        transacType  = parent?.getItemAtPosition(position).toString()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        // Handle case where no item is selected
    }

    fun getUID(): String{
        return firebaseAuth.auth.currentUser!!.uid
    }

    private fun saveDataToFirebase() {
        var transac  = Transaction("",type, tanggal, nominalStr, desc)

        db.addTransaction(transac, getUID()){ callback ->
            if (callback){
                Log.d("Firebase", "Data berhasil disimpan di Firebase")
                startActivity(Intent( this, MainActivity::class.java
                ))
                finish()
            }else{
                Log.w("Firebase", "Error saat menyimpan data di Firebase")
            }
        }

    }

    private fun showDatePickerDialog() {
        val datePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                // Menangani pemilihan tanggal
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                // Format tanggal yang dipilih
                val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                etTanggal.setText(dateFormat.format(calendar.time))
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }
}




