package com.papb.moneyapp

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.papb.moneyapp.model.Transaction
import com.papb.moneyapp.Helper

import androidx.core.content.ContextCompat.startActivity
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale



class FinancialAdapter(private val financialDataList: List<Transaction>) :
    RecyclerView.Adapter<FinancialAdapter.FinancialViewHolder>() {

    private val calendar = Calendar.getInstance()
    val helper = Helper()
    // Inner class ViewHolder untuk menyimpan referensi ke elemen dalam item tampilan
    class FinancialViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)
        val amountTextView: TextView = itemView.findViewById(R.id.amountTextView)
        val tvDate : TextView = itemView.findViewById(R.id.dateTextView)
        val iconDelete : ImageView = itemView.findViewById(R.id.iconDelete)
        // Definisikan referensi ke elemen-elemen lain dalam item tampilan di sini
    }

    // Implementasikan metode-metode yang diperlukan dalam adapter
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FinancialViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_financial, parent, false)
        return FinancialViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FinancialViewHolder, position: Int) {
        val transacData :Transaction = financialDataList[position]

        holder.titleTextView.text = transacData.desc

        holder.tvDate.text = helper.dateFormatter(transacData.date)

        if (transacData.type == "Income") {
            holder.amountTextView.setTextColor(Color.GREEN)
            holder.amountTextView.text = "+ " + helper.currency(transacData.nominal)
        } else if (transacData.type == "Expense") {
            holder.amountTextView.setTextColor(Color.RED)
            holder.amountTextView.text = "- " + helper.currency(transacData.nominal)
        }

        // Di dalam metode onBindViewHolder pada adapter Anda
        holder.iconDelete.setOnClickListener {
            (holder.iconDelete.context as MainActivity).deleteItemFromDatabase(position, transacData.docId)
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, AddDataActivity::class.java)

            intent.putExtra("position", position)
            intent.putExtra("uid", transacData.uid)
            intent.putExtra("type", transacData.type)
            intent.putExtra("date", transacData.date)
            intent.putExtra("nominal", transacData.nominal)
            intent.putExtra("desc", transacData.desc)
            intent.putExtra("docId", transacData.docId)
            holder.itemView.context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return financialDataList.size
    }

//    fun showUpdateDialog(transaction: Transaction, position: Int, uid: String) {
//        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_update, null)
//        val builder = AlertDialog.Builder(this)
//            .setView(dialogView)
//            .setTitle("Update Data")
//
//        val etDesc = dialogView.findViewById<EditText>(R.id.etDesc)
//        val etNominal = dialogView.findViewById<EditText>(R.id.etAmount)
//        val etTanggal = dialogView.findViewById<EditText>(R.id.etTanggal)
////        val spinnerType = dialogView.findViewById<Spinner>(R.id.spinnerType)
//
//        etDesc.setText(transaction.desc)
//        etNominal.setText(transaction.nominal.toString())
//
////        val transType = dialogView.resources.getStringArray(R.array.type)
////        val adapter = ArrayAdapter(this, R.layout.dropdown_item, transType)
////        spinnerType.adapter = adapter
////        spinnerType.setSelection(adapter.getPosition(transaction.type))
//
//        val transType = dialogView.resources.getStringArray(R.array.type)
//        val adapter = ArrayAdapter(dialogView.context, R.layout.dropdown_item, transType)
//
//        // Get spinner view and set adapter
//        val spinner = dialogView.findViewById<Spinner>(R.id.dropdown_type)
//        spinner.adapter = adapter
//
//        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                // Your code here
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                // Your code here
//            }
//        }
//
//
//        // Menangani klik pada TextInputEditText etTanggal
//        etTanggal.setOnClickListener {
//            showDatePickerDialog()
//        }
//
//        builder.setPositiveButton("Update") { _, _ ->
//            val newDesc = etDesc.text.toString()
//            val newNominal = etNominal.text.toString().toDouble()
////            val newType = spinnerType.selectedItem.toString()
//
//            val newData = hashMapOf(
//                "desc" to newDesc,
//                "nominal" to newNominal,
//                "date" to newType
//            )
//
//            MainActivity().updateDataInFirestore(position, uid, newData)
//        }
//
//        builder.setNegativeButton("Cancel", null)
//
//        val dialog = builder.create()
//        dialog.show()
//    }






}
