package com.papb.moneyapp.firebase

import android.util.Log
import android.widget.Toast
import android.content.ContentValues.TAG
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.data.model.User
import com.google.firebase.FirebaseOptions
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.database
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.papb.moneyapp.R
import com.papb.moneyapp.model.Transaction
import com.papb.moneyapp.model.Users

class DbRealtime : AppCompatActivity() {


    val database = Firebase.firestore




    fun insertUser(user : Users, uid : String) {

        val userMap = hashMapOf(
            "name" to user.nama,
            "email" to user.email,
            "password" to user.password,
            "age" to user.age
        )
        if (uid.isNotEmpty()){
            database.collection("users").document(uid).set(userMap)
                .addOnFailureListener{
                    Toast.makeText(
                        this,
                        "Berhasil!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
        }

    }

    fun addTransaction(transaction: Transaction, uid : String, callback: (Boolean) -> Unit){
        val transacMap = hashMapOf(
            "uid" to uid,
            "type" to transaction.type,
            "date" to transaction.date,
            "nominal" to transaction.nominal,
            "desc" to transaction.desc

        )
        database.collection("transaction").document().set(transacMap)
            .addOnCompleteListener{ task->
                if (task.isSuccessful) {
                    callback(true)
                } else {
                    callback(false) // Store failed
                }
            }
    }

    fun updateDataInFirestore(position: Int, uid: String, transaction: Transaction, callback: (Boolean) -> Unit) {
        val collectionRef = database.collection("transaction")
        val transacMap = hashMapOf(
            "uid" to uid,
            "type" to transaction.type,
            "date" to transaction.date,
            "nominal" to transaction.nominal,
            "desc" to transaction.desc

        )
        collectionRef
            .document(transaction.docId).set(transacMap)
            .addOnSuccessListener {
                callback(true)
            }
            .addOnFailureListener { e ->
                Log.e("Firestore", "Error deleting document", e)
                Toast.makeText(this, "Failed to delete item", Toast.LENGTH_SHORT).show()
                callback(false)
            }
    }


}