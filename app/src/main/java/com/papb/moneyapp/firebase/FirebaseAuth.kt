package com.papb.moneyapp.firebase

import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.papb.moneyapp.MainActivity
import com.papb.moneyapp.model.Users


class FirebaseAuth : AppCompatActivity(){
    var auth = FirebaseAuth.getInstance()
    val firebaseUser = auth.currentUser
    private val db = DbRealtime()

    fun updateUI(context: Context){
        val currentUser = auth.currentUser
        if (currentUser != null) {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
    }



    fun signIn(email: String, password: String, callback: (Boolean) -> Unit){
        auth.signInWithEmailAndPassword(email,password)
            .addOnCompleteListener{ task->
                if (task.isSuccessful) {
                    callback(true)
                } else {
                    callback(false) // Login failed
                }
            }
    }

    fun signUp(email: String, password: String, nama: String, age: String, callback: (Boolean) -> Unit) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    callback(true)
                    db.insertUser(Users(nama, email, password, age), auth.currentUser!!.uid)
                } else {
                    callback(false)
                }
            }
    }
    fun logout(){
        auth.signOut()
    }




}