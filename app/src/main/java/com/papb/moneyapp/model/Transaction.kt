package com.papb.moneyapp.model

import java.sql.Timestamp
import java.util.Date

data class Transaction(
    var uid : String ="",
    var type:String ="",
    var date: String ="",
    var nominal: String ="",
    var desc: String ="",
    var docId: String = ""
)
