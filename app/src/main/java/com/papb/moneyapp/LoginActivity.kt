package com.papb.moneyapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Email
import android.widget.EditText
import android.widget.Button
import android.widget.Toast
import com.papb.moneyapp.firebase.FirebaseAuth
import com.google.android.play.core.integrity.v

class LoginActivity : AppCompatActivity() {

    val firebaseAuth = FirebaseAuth()
    override fun onStart() {
        super.onStart()
        if(firebaseAuth.firebaseUser!=null){
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val editEmail = findViewById<EditText>(R.id.emailLogin)
        val editPassword = findViewById<EditText>(R.id.password)
        val btnLogin = findViewById<Button>(R.id.btn_login)
        val btnRegister = findViewById<Button>(R.id.btn_register)


        btnRegister.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RegistActivity::class.java))
        }

        btnLogin.setOnClickListener {
            val email = editEmail.text.toString()
            val password = editPassword.text.toString()
            if (editEmail.text.isNotEmpty() && editPassword.text.isNotEmpty()){
                //LAUNCH LOGIN
                signInUser(email, password)
            }else{
                Toast.makeText(this,"Masukan Email dan Password terlebih dahulu!",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun signInUser(email:String, password : String){
        firebaseAuth.signIn(email, password) { success ->
            if(success){
                Toast.makeText(
                    this@LoginActivity,
                    "Login Berhasil!",
                    Toast.LENGTH_SHORT
                ).show()
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
            }
            else{
                Toast.makeText(
                    this@LoginActivity,
                    "Login Gagal!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }


}