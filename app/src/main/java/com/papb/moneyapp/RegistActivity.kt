package com.papb.moneyapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.papb.moneyapp.model.Users


import com.papb.moneyapp.firebase.DbRealtime
import com.papb.moneyapp.firebase.FirebaseAuth

class RegistActivity : AppCompatActivity() {
    private lateinit var editFullName : EditText
    private lateinit var editEmail : EditText
    private lateinit var editPassword : EditText
    private lateinit var editPasswordConf: EditText
    private lateinit var editAge: EditText
    private lateinit var btnRegister :Button
    private lateinit var btnLogin : Button
    private val firebaseAuth = FirebaseAuth()
    private val database = DbRealtime()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_regist)
        editFullName = findViewById(R.id.name)
        editEmail =findViewById(R.id.email)
        editPassword = findViewById(R.id.password)
        editPasswordConf = findViewById(R.id.password_conf)
        editAge = findViewById(R.id.usia)
        btnLogin = findViewById(R.id.btn_login)
        btnRegister = findViewById(R.id.btn_register)





        btnLogin.setOnClickListener {
            val intent = Intent(this@RegistActivity, MainActivity::class.java)
            startActivity(intent)
        }
        btnRegister.setOnClickListener{
            val nama = editFullName.text.toString()
            val email = editEmail.text.toString()
            val password = editPassword.text.toString()
            val ageraw = editAge.text.toString()
            var ageInt = ageraw.toInt()
            if (ageInt>80){
                ageInt = 80
            }else if (ageInt<1){
                ageInt = 1
            }
            if (editFullName.text.isNotEmpty() && editEmail.text.isNotEmpty() && editPassword.text.isNotEmpty()){
                if (editPassword.text.toString() == editPasswordConf.text.toString()){

                    //LAUNCH REGISTER
                    signUpUser(nama, email, password, ageInt.toString())


                }else{
                    Toast.makeText(this,"Password yangdimasukan tidak sama!", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this,"Masukan data dengan lengkap!",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun signUpUser(nama : String, email : String, password : String, age : String){
        firebaseAuth.signUp(email,password, nama, age){success ->
            if(success){
                Toast.makeText(
                    this@RegistActivity,
                    resources.getString(R.string.signup_success),
                    Toast.LENGTH_SHORT
                ).show()


                val intent = Intent(this@RegistActivity, LoginActivity::class.java)
                startActivity(intent)
            }
            else{
                Toast.makeText(
                    this@RegistActivity,
                    resources.getString(R.string.signup_failed),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }


}