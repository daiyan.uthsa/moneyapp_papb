package com.papb.moneyapp

data class FinancialData(
    var title: String? = null,
    var amount: Double? = null,
    var date: String? = null
) {
    // Konstruktor tanpa argumen
    constructor() : this("", 0.0, "")
}